using System.Collections;
using Script.ForMenu;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


namespace Script.ForGame2
{
    public class GameManagerForPhysicsGameTwo : MonoBehaviour
    {
        public RectTransform menuBox;
        public float transitionTime = 1f;
        private int screenHight;

        private void Start()
        {
            screenHight = Screen.height;
        }
        
        private void ChangeMenu(MenuType menuTpye)
        {
            Vector3 newPos;
            if (menuTpye == MenuType.SettingPower)
            {
                newPos = new Vector3(0f,screenHight,0f);
            }
            else
            {
                newPos = Vector3.zero;
            }
            StopAllCoroutines();
            StartCoroutine(ChageMenuAnimation(newPos));
        }
        private IEnumerator ChageMenuAnimation(Vector3 newPos)
        {
            float shiftingTime = 0f;
            Vector3 oldPos = menuBox.anchoredPosition3D;

            while (shiftingTime <= transitionTime)
            {
                shiftingTime += Time.deltaTime;
                Vector3 currentPos = Vector3.Lerp(oldPos, newPos, shiftingTime / transitionTime);
                menuBox.anchoredPosition3D = currentPos;
                yield return null;
            }
        }
        public void OnSettingPowerButtonClicked()
        {
            ChangeMenu(MenuType.SettingPower);
            MenuSound.clickInstance.audio.PlayOneShot(MenuSound.clickInstance.click);
        }

        public void OnMainMenuButtonClicked()
        {
            ChangeMenu(MenuType.Mainmenu);
            MenuSound.clickInstance.audio.PlayOneShot(MenuSound.clickInstance.click);
        }
        public void OnExitButtonClicked() 
        {
            SceneManager.LoadScene("MenuScene"); 
            MenuSound.clickInstance.audio.PlayOneShot(MenuSound.clickInstance.click);
        }
        public void OnRestartClicked()
        {
            SceneManager.LoadScene("PhysicGame2");
            MenuSound.clickInstance.audio.PlayOneShot(MenuSound.clickInstance.click);
        }
        private enum MenuType
        {    
            Mainmenu,
            SettingPower,
        }
    }
}
