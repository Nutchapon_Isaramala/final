using TMPro;
using UnityEngine;

namespace Script.ForGame2
{
    public class CheckHit : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI mainQuest;
        [SerializeField] private GameObject correctCube;
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag.Equals("YellowCube"))
            {
                Destroy(other.gameObject);
            }
            else if (correctCube.tag.Equals("CorrectYellowCube"))
            {
                Destroy(correctCube);
                mainQuest.text = "Go to Red Cube";
            }
        }
    }
}
