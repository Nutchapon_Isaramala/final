using UnityEngine;
using Random = UnityEngine.Random;

namespace Script.ForGame2
{
    public class Block : MonoBehaviour
    {
        [SerializeField] private GameObject[] block;
        public float spawnOneTime;
        public float spawnOneTime2 = 1;
        private void FixedUpdate()
        {
           Cooldowns();
        }
        private void Spawn()
        {
            var random = Random.Range(1,5);
            switch (random)
                {
                    case 1:
                        Instantiate(block[0]);
                        break;
                    case 2:
                        Instantiate(block[1]);
                        break;
                    case 3:
                        Instantiate(block[2]);
                        break;
                    case 4:
                        Instantiate(block[3]);
                        break;
                    case 5:
                        Instantiate(block[4]);
                        break;

                }
        }
        private void Cooldowns()
        {
            
            if (Input.GetKey(KeyCode.Space) && spawnOneTime == 0)
            {
                Spawn();
                spawnOneTime = spawnOneTime2;
            }
        }
    }
}