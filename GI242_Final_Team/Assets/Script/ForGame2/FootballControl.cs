using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Script.ForGame2
{
    public class FootballControl : MonoBehaviour
    {
        [SerializeField] private Vector3 angularVelocity, velocity;
        [SerializeField] private TextMeshProUGUI powerData;
        [SerializeField] private TextMeshProUGUI GoalText;
        public GameObject []invisibleblock;
        private Rigidbody rb;
        public float speed;
        public float canShoot;
        public float cantShoot = 1;
        public float canmove;
        public float cantmove = 1;
        
        [SerializeField] private AudioClip playerShoot;
        [SerializeField] private float playerShootdSoundVolume = 1f;

        public TMP_InputField angularVelocityX;
        public TMP_InputField angularVelocityY;
        public TMP_InputField angularVelocityZ;
        public TMP_InputField velocityX;
        public TMP_InputField velocityY;
        public TMP_InputField velocityZ;
        void Start()
        {
            rb = GetComponent<Rigidbody>();
        }
        void FixedUpdate()
        {
            DeleteBlock();
            Shoot();   
            Movement();
            powerData.text = $"Angular Velocity : [X] =  {angularVelocity.x} [Y] =  {angularVelocity.y} [Z] =  {angularVelocity.z} " +
                             $"\n      Velocity :        [X] =  {velocity.x} [Y] =  {velocity.y} [Z] =  {velocity.z}";

        }
        public void SaveData()
        {
            Vector3 angularVelocity =  new Vector3();
            Vector3 velocity = new Vector3();
            angularVelocity.x = string.IsNullOrEmpty(angularVelocityX.text) ? 0 : Convert.ToSingle(angularVelocityX.text);
            angularVelocity.y = string.IsNullOrEmpty(angularVelocityY.text) ? 0 : Convert.ToSingle(angularVelocityY.text);
            angularVelocity.z = string.IsNullOrEmpty(angularVelocityZ.text) ? 0 : Convert.ToSingle(angularVelocityZ.text);
            velocity.x = string.IsNullOrEmpty(velocityX.text) ? 0 : Convert.ToSingle(velocityX.text);
            velocity.y = string.IsNullOrEmpty(velocityY.text) ? 0 : Convert.ToSingle(velocityY.text);
            velocity.z = string.IsNullOrEmpty(velocityZ.text) ? 0 : Convert.ToSingle(velocityZ.text);
            this.angularVelocity = angularVelocity;
            this.velocity = velocity;
        }
        private void Movement()
        {
            if (canmove == 0)
            {
                rb.AddTorque(Input.GetAxis("Horizontal") * -transform.forward * speed);
            }
        }
        private void Shoot()
        {
            if (Input.GetKey(KeyCode.Space) && canShoot == 0)
            {
                canmove = cantmove;
                rb.angularVelocity = angularVelocity;               
                rb.velocity = velocity;
                canShoot = cantShoot;
                AudioSource.PlayClipAtPoint(playerShoot, Camera.main.transform.position, playerShootdSoundVolume);
            }
            rb.AddForce(Vector3.Cross(rb.angularVelocity, rb.velocity));
        }

        private void DeleteBlock()
        {
            if (Input.GetKey(KeyCode.Space))
            {
                Destroy(invisibleblock[0]);
                Destroy(invisibleblock[1]);
                Destroy(invisibleblock[2]);
                Destroy(invisibleblock[3]);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag.Equals("CheckHit"))
            {
                GoalText.text = "GOAL";
            }
        }
    }
}
