using UnityEngine;

namespace Script.ForGame3
{
    public class DeleteBall : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag.Equals("ball"))
            {
               Destroy(other.gameObject);
            }
        }
    }
}
