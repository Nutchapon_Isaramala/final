﻿using UnityEngine;

namespace Script.ForGame3
{
    public class Projectile : MonoBehaviour
    {
        [SerializeField] private Rigidbody bulletPrefabs;
        [SerializeField] private GameObject cursor;
        [SerializeField] private LayerMask layer;
        [SerializeField] private Transform shootPoint;

        private Camera _camera;
    
        void Start()
        {
            _camera = Camera.main;
        }


        void Update()
        {
            LunchProjectile();
        }

        void LunchProjectile()
        {
            Ray camerayRay = _camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(camerayRay, out hit, 100f, layer))
            {
                cursor.SetActive(true);
                cursor.transform.position = hit.point + Vector3.up * 0.03f;

                Vector3 V_originXY = CalculateVelocity(hit.point, shootPoint.position, 1f);

                transform.rotation = Quaternion.LookRotation(V_originXY);

                if (Input.GetMouseButtonDown(0))
                {
                    Rigidbody fire = Instantiate(bulletPrefabs, shootPoint.position , Quaternion.identity);
                    fire.velocity = V_originXY;
                }
            }
            else
            {
                cursor.SetActive(false);
            }

            Vector3 CalculateVelocity(Vector3 target, Vector3 origin, float time)
            {
                Vector3 distance = target - origin;
                Vector3 distanceXZ_plane = distance;
                distanceXZ_plane.y = 0f;

                float distanceXZ = distanceXZ_plane.magnitude;
                float distanceY = distance.y;

                float Vxz = distanceXZ / time;
                float Vy = distanceY / time + 0.5f * Mathf.Abs(Physics.gravity.y) * time;

                Vector3 result = distanceXZ_plane.normalized;
                result *= Vxz;
                result.y = Vy;

                return result;
            }
        }//update

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "ball")
            {
                ScoreArea.sp.scoreCount();
            }
        }
    }
}//pj
