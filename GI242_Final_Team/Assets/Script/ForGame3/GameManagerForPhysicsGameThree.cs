using System;
using Script.ForMenu;
using UnityEngine;
using UnityEngine.SceneManagement;
namespace Script.ForGame3
{
    public class GameManagerForPhysicsGameThree : MonoBehaviour
    {
        
        public void FixedUpdate()
        {
            if (Input.GetKey(KeyCode.R))
            {
                SceneManager.LoadScene("PhysicGame3"); 
                MenuSound.clickInstance.audio.PlayOneShot(MenuSound.clickInstance.click);
            }
            if (Input.GetKey(KeyCode.E))
            {
                SceneManager.LoadScene("MenuScene"); 
                MenuSound.clickInstance.audio.PlayOneShot(MenuSound.clickInstance.click);
                Cursor.lockState = CursorLockMode.None;
            }
        }
        public void OnExitButtonClicked() 
        {
            SceneManager.LoadScene("MenuScene"); 
            MenuSound.clickInstance.audio.PlayOneShot(MenuSound.clickInstance.click);
        }
        public void OnRestartClicked()
        {        
            SceneManager.LoadScene("PhysicGame3");
            MenuSound.clickInstance.audio.PlayOneShot(MenuSound.clickInstance.click);
        }
    }
}
