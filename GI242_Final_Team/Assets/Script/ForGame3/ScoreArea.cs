﻿using TMPro;
using UnityEngine;

namespace Script.ForGame3
{
    public class ScoreArea : MonoBehaviour
    {
        [SerializeField] private AudioClip getScore;
        [SerializeField] private float getScoreSoundVolume = 1f;
        private int score = 0;
        public TextMeshProUGUI scoreUI;
        public static ScoreArea sp;
        
        void Start()
        {
            sp = this;
        }
        private void OnTriggerEnter(Collider otherCollider)
        {
            if (otherCollider.tag == "ball")
            {
                AudioSource.PlayClipAtPoint(getScore, Camera.main.transform.position, getScoreSoundVolume);
                score++;
                Destroy(otherCollider.gameObject);
                scoreUI.text = "Score : " + score;
            }
        }
        public void scoreCount()
        {
            score = score +1;         
            scoreUI.text = "score : " + score;
        }
    }
}
