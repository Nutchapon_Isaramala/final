using UnityEngine;

namespace Script.ForMenu
{
    public class MenuSound : MonoBehaviour
    {
        public new AudioSource audio;
        public AudioClip click;
        public static MenuSound clickInstance;

        private void Awake()
        {
            if (clickInstance != null && clickInstance != this) 
            {
                Destroy(this.gameObject);
                return;
            }

            clickInstance = this;
            DontDestroyOnLoad(this);
        }
    }
}
