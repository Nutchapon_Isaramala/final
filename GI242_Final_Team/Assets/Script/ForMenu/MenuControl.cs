using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Script.ForMenu
{
    public class MenuControl : MonoBehaviour
    {
        public Transform gameStorage;
        public RectTransform menuStorage;
        public float transitionTime = 1f;
        private int screenHight;
        private int screenWidth;
        
       

        private void Start()
        {
            InitLevelButton();
            screenHight = Screen.height;
            screenWidth = Screen.width;
        }

        private void InitLevelButton()
        {
            int i = 0;
            foreach (Transform t in gameStorage)
            {
                int numberGame = i;
                Button button = t.GetComponent<Button>();
                button.onClick.AddListener(() => OnNumberSelect(numberGame));
                i++;
            }
        }
        private void ChangeMenu(MenuType menuTpye)
        {
            Vector3 newPos;
            if (menuTpye == MenuType.SelectMapMenu)
            {
                newPos = new Vector3(0f,screenHight,0f);
            }
            else if (menuTpye == MenuType.CreditsMenu)
            {
                newPos = new Vector3(-screenWidth,0f,0f);
            }
            else
            {
                newPos = Vector3.zero;
            }
            StopAllCoroutines();
            StartCoroutine(ChageMenuAnimation(newPos));
        }
        private IEnumerator ChageMenuAnimation(Vector3 newPos)
        {
            float shiftingTime = 0f;
            Vector3 oldPos = menuStorage.anchoredPosition3D;

            while (shiftingTime <= transitionTime)
            {
                shiftingTime += Time.deltaTime;
                Vector3 currentPos = Vector3.Lerp(oldPos, newPos, shiftingTime / transitionTime);
                menuStorage.anchoredPosition3D = currentPos;
                yield return null;
            }
        }
        private void OnNumberSelect(int numberGame)
        {
            numberGame += 1;
            string sceneName = "PhysicGame" + numberGame;
            SceneManager.LoadScene(sceneName);
            MenuSound.clickInstance.audio.PlayOneShot(MenuSound.clickInstance.click);
        }
        public void OnPlayButtonClicked()
        {
            ChangeMenu(MenuType.SelectMapMenu);
            MenuSound.clickInstance.audio.PlayOneShot(MenuSound.clickInstance.click);
        }

        public void OnMainMenuButtonClicked()
        {
            ChangeMenu(MenuType.Mainmenu);
            MenuSound.clickInstance.audio.PlayOneShot(MenuSound.clickInstance.click);
        }
        public void OnCreditsButtonClicked()
        {
            ChangeMenu(MenuType.CreditsMenu);
            MenuSound.clickInstance.audio.PlayOneShot(MenuSound.clickInstance.click);
        }

        public void Quit()
        {
            Application.Quit();
        }
        private enum MenuType
        {    
            Mainmenu,
            SelectMapMenu,
            CreditsMenu
        }
    }
}
