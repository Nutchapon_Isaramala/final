using System;
using Script.ForMenu;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Script.ForGame1
{
    public class GameManagerForPhysicsGameOne : MonoBehaviour
    {
        public void OnExitButtonClicked() 
        {
            SceneManager.LoadScene("MenuScene"); 
            MenuSound.clickInstance.audio.PlayOneShot(MenuSound.clickInstance.click);
        }
        public void OnRestartClicked()
        {        
            SceneManager.LoadScene("PhysicGame1");
            MenuSound.clickInstance.audio.PlayOneShot(MenuSound.clickInstance.click);
        }
    }
}
