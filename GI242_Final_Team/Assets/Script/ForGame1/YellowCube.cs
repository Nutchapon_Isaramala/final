using TMPro;
using UnityEngine;

namespace Script.ForGame1
{
    public class YellowCube : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI mainQuest;
        [SerializeField] private GameObject correctCube;
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag.Equals("YellowCube"))
            {
                Destroy(other.gameObject);
            }
            else if (gameObject.tag.Equals("CorrectYellowCube"))
            {
                Destroy(correctCube);
                mainQuest.text = "Go to Red Cube";
            }
        }
    }
}
