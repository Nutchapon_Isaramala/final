using TMPro;
using UnityEngine;

namespace Script.ForGame1
{
    public class RedCube : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI completeText;
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag.Equals("RedCube"))
            {
                completeText.text = "Complete";
            }
        }
    }
}
