using UnityEngine;

namespace Script.ForGame1
{
    public class BulletControl : MonoBehaviour
    {
        [SerializeField] private Transform []spawnpoints;
        [SerializeField] private Rigidbody bullet;
        [SerializeField] private AudioClip playerShoot;
        [SerializeField] private float playerShootSoundVolume = 1f;

    
        public float bulletSpeed = 500f;
        public float reloadBulletTime;
        public float reloadRemaining;
        
    
        void FixedUpdate()
        {
            if (reloadRemaining > 0)
            {
                reloadRemaining -= Time.deltaTime;
            }
            if (reloadRemaining < 0)
            {
                reloadRemaining = 0;
            }
            if (Input.GetKey(KeyCode.F) && reloadRemaining == 0)
            {
                Fire();
                reloadRemaining = reloadBulletTime;
            }
        }

        private void Fire()
        {
            foreach (var spawnpoint in spawnpoints)
            {
                var shoot = Instantiate(bullet, spawnpoint.position, spawnpoint.rotation);
                shoot.AddForce(spawnpoint.forward * bulletSpeed);
                AudioSource.PlayClipAtPoint(playerShoot, Camera.main.transform.position, playerShootSoundVolume);
            }
        }
    
    }
}
