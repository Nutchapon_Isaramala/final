using UnityEngine;

namespace Script.ForGame1
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private AudioClip playerBoost;
        [SerializeField] private float playerBoostdSoundVolume = 1f;
        
        public float trustPower, liftBooster;
        private Rigidbody rb;
        void Start()
        {
            rb = GetComponent<Rigidbody>();
        }

        void FixedUpdate()
        {
            Move();
        }
        private void Move()
        {
            if (Input.GetKey(KeyCode.Space))
            {
                rb.AddForce(transform.forward * trustPower);
                
                Vector3 lift = Vector3.Project(rb.velocity, transform.position);
                rb.AddForce(transform.up * lift.magnitude * liftBooster);
                
                AudioSource.PlayClipAtPoint(playerBoost, Camera.main.transform.position, playerBoostdSoundVolume);
            }
            rb.drag = rb.velocity.magnitude * 0.0001f;
            rb.angularDrag = rb.velocity.magnitude * 0.01f;
            
            if (Input.GetKey(KeyCode.A))
            {
                rb.AddTorque(transform.forward * Time.deltaTime * 10f);
            }
 
            if (Input.GetKey(KeyCode.D))
            {
                rb.AddTorque(-transform.forward * Time.deltaTime * 10f);
          
            }
            if (Input.GetKey(KeyCode.W))
            {
                rb.AddTorque(transform.right * Time.deltaTime * 10f);
            }
            if (Input.GetKey(KeyCode.S))
            {
                rb.AddTorque(-transform.right * Time.deltaTime * 10f);
            }
        }
    }
}

