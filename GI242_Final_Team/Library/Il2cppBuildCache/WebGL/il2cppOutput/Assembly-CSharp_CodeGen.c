﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Script.ForMenu.MenuControl::Start()
extern void MenuControl_Start_mD21964394815531B6A097339BA0A03ABF4809F30 (void);
// 0x00000002 System.Void Script.ForMenu.MenuControl::InitLevelButton()
extern void MenuControl_InitLevelButton_mE719A48CEF2CB7E67D9625583807D00D1C3C7DB0 (void);
// 0x00000003 System.Void Script.ForMenu.MenuControl::ChangeMenu(Script.ForMenu.MenuControl/MenuType)
extern void MenuControl_ChangeMenu_m20609C8E4955A57406AE7CDF80B8A207DBC0D4E6 (void);
// 0x00000004 System.Collections.IEnumerator Script.ForMenu.MenuControl::ChageMenuAnimation(UnityEngine.Vector3)
extern void MenuControl_ChageMenuAnimation_mAF201D60BB6A22E57A3698BB07E80927111B8FF5 (void);
// 0x00000005 System.Void Script.ForMenu.MenuControl::OnNumberSelect(System.Int32)
extern void MenuControl_OnNumberSelect_mD33B819701C033AFE67F26590D985438BC2276BD (void);
// 0x00000006 System.Void Script.ForMenu.MenuControl::OnPlayButtonClicked()
extern void MenuControl_OnPlayButtonClicked_m3089A9C0309C357380BF7CE845CED7B47054989F (void);
// 0x00000007 System.Void Script.ForMenu.MenuControl::OnMainMenuButtonClicked()
extern void MenuControl_OnMainMenuButtonClicked_m748D718CC10175DE8AF551AB1AA50B75FBFA1BE2 (void);
// 0x00000008 System.Void Script.ForMenu.MenuControl::OnCreditsButtonClicked()
extern void MenuControl_OnCreditsButtonClicked_m213C6097EAE48714AD2AEA8CC12383EFFF95AAAE (void);
// 0x00000009 System.Void Script.ForMenu.MenuControl::Quit()
extern void MenuControl_Quit_m9400004B264D91198F1C6AFE6BEFED420D6F01FD (void);
// 0x0000000A System.Void Script.ForMenu.MenuControl::.ctor()
extern void MenuControl__ctor_mA26ECD3901C87B4AB5E9791AFB59D40FA3079166 (void);
// 0x0000000B System.Void Script.ForMenu.MenuControl/<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m414E3E3630FCEBF3107F5A302085F366EB0D1958 (void);
// 0x0000000C System.Void Script.ForMenu.MenuControl/<>c__DisplayClass6_0::<InitLevelButton>b__0()
extern void U3CU3Ec__DisplayClass6_0_U3CInitLevelButtonU3Eb__0_mC63D3EEAFEB20BD38FAE870471FDBE040035C7C7 (void);
// 0x0000000D System.Void Script.ForMenu.MenuControl/<ChageMenuAnimation>d__8::.ctor(System.Int32)
extern void U3CChageMenuAnimationU3Ed__8__ctor_mBFB3BD211076C6EE28A15B82D5F83A883B483B50 (void);
// 0x0000000E System.Void Script.ForMenu.MenuControl/<ChageMenuAnimation>d__8::System.IDisposable.Dispose()
extern void U3CChageMenuAnimationU3Ed__8_System_IDisposable_Dispose_mB512C8B02E3C3B5AEC0467D9A3393175586CAD45 (void);
// 0x0000000F System.Boolean Script.ForMenu.MenuControl/<ChageMenuAnimation>d__8::MoveNext()
extern void U3CChageMenuAnimationU3Ed__8_MoveNext_mCB4C2F21463D83F21A260163BEA58FAA8E9C346F (void);
// 0x00000010 System.Object Script.ForMenu.MenuControl/<ChageMenuAnimation>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CChageMenuAnimationU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC5F748F0211E70047D28296E465BFAA02294C011 (void);
// 0x00000011 System.Void Script.ForMenu.MenuControl/<ChageMenuAnimation>d__8::System.Collections.IEnumerator.Reset()
extern void U3CChageMenuAnimationU3Ed__8_System_Collections_IEnumerator_Reset_m9F62FFDE0B9D9FC68CD7D709F4F5E650EC671D81 (void);
// 0x00000012 System.Object Script.ForMenu.MenuControl/<ChageMenuAnimation>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CChageMenuAnimationU3Ed__8_System_Collections_IEnumerator_get_Current_m3B641347E2DD11B0A2999A902D1A5F2FF81C8AD9 (void);
// 0x00000013 System.Void Script.ForMenu.MenuSound::Awake()
extern void MenuSound_Awake_mAB024C294FD0DEACA600BF2E46E6B189A37D843B (void);
// 0x00000014 System.Void Script.ForMenu.MenuSound::.ctor()
extern void MenuSound__ctor_m04E1F731E6AB3FB0B9D46FEBF54DDEF376ADD8B8 (void);
// 0x00000015 System.Void Script.ForGame3.BulletScript::Start()
extern void BulletScript_Start_mA34DCEA3EEF4B4C59C7894A2BF88A2CBB3E41511 (void);
// 0x00000016 System.Void Script.ForGame3.BulletScript::Update()
extern void BulletScript_Update_m496194453396775E3E37212315732887208C1944 (void);
// 0x00000017 System.Void Script.ForGame3.BulletScript::LunchProjectile()
extern void BulletScript_LunchProjectile_m9EF6D6E2D58252EA1A988DC6C2E6968498AEFD00 (void);
// 0x00000018 System.Void Script.ForGame3.BulletScript::.ctor()
extern void BulletScript__ctor_m20F8377E387D8E55B9CE61D63A6F120491921887 (void);
// 0x00000019 UnityEngine.Vector3 Script.ForGame3.BulletScript::<LunchProjectile>g__CalculateVelocityU7C9_0(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void BulletScript_U3CLunchProjectileU3Eg__CalculateVelocityU7C9_0_m2B6CBE5A42025E49EE25EBAA29D6E5A8FE8382A9 (void);
// 0x0000001A System.Void Script.ForGame3.DeleteBall::OnTriggerEnter(UnityEngine.Collider)
extern void DeleteBall_OnTriggerEnter_m6DC52D6F2528718C4A3819C244E6E069EF45B815 (void);
// 0x0000001B System.Void Script.ForGame3.DeleteBall::.ctor()
extern void DeleteBall__ctor_mBAA6EB20A436C2E5048D523B0EC13C4060F3CDA3 (void);
// 0x0000001C System.Void Script.ForGame3.GameManagerForPhysicsGameThree::FixedUpdate()
extern void GameManagerForPhysicsGameThree_FixedUpdate_m84A94F5FB54763D12AF7F336BCC423EE3B272F0C (void);
// 0x0000001D System.Void Script.ForGame3.GameManagerForPhysicsGameThree::OnExitButtonClicked()
extern void GameManagerForPhysicsGameThree_OnExitButtonClicked_m8CA1ED1E3FB7844ECE033307CA6852C9688C9123 (void);
// 0x0000001E System.Void Script.ForGame3.GameManagerForPhysicsGameThree::OnRestartClicked()
extern void GameManagerForPhysicsGameThree_OnRestartClicked_m8D4AF0AFBD7F252935196699644154C05C5276EC (void);
// 0x0000001F System.Void Script.ForGame3.GameManagerForPhysicsGameThree::.ctor()
extern void GameManagerForPhysicsGameThree__ctor_m58FD7CC710E8E77CDC40D3914C0E7BA3D5B07DEB (void);
// 0x00000020 System.Void Script.ForGame3.MouseLook::Start()
extern void MouseLook_Start_m514114534D96AC00B531892234D557EA58F3477E (void);
// 0x00000021 System.Void Script.ForGame3.MouseLook::Update()
extern void MouseLook_Update_mFFAFF4FD2868081BE4503937F387FF7AB5FCCC5A (void);
// 0x00000022 System.Void Script.ForGame3.MouseLook::.ctor()
extern void MouseLook__ctor_m9396072602E811AB09C7B5C03D19E53A77C00BB5 (void);
// 0x00000023 System.Void Script.ForGame3.PlayerMovement::Update()
extern void PlayerMovement_Update_m241BC416BFDC91F1C8AF01230836C4C37B21082B (void);
// 0x00000024 System.Void Script.ForGame3.PlayerMovement::.ctor()
extern void PlayerMovement__ctor_m0A7C744B933C4B2C145863109D88BFD6A4375453 (void);
// 0x00000025 System.Void Script.ForGame3.Projectile::Start()
extern void Projectile_Start_mE3048ED065CD6782748B980B2C56B323E5030774 (void);
// 0x00000026 System.Void Script.ForGame3.Projectile::Update()
extern void Projectile_Update_m49DC62A3C68EB106AD225059EC2A8FC194077ED7 (void);
// 0x00000027 System.Void Script.ForGame3.Projectile::LunchProjectile()
extern void Projectile_LunchProjectile_m3E40E37398D764B65591C76B4B74E2E050587764 (void);
// 0x00000028 System.Void Script.ForGame3.Projectile::OnTriggerEnter(UnityEngine.Collider)
extern void Projectile_OnTriggerEnter_m522E8CDC74F5C291240BB919C16923E2851BB3F9 (void);
// 0x00000029 System.Void Script.ForGame3.Projectile::.ctor()
extern void Projectile__ctor_m593C60F79D70FC56CBA9374281744FA43017EF48 (void);
// 0x0000002A UnityEngine.Vector3 Script.ForGame3.Projectile::<LunchProjectile>g__CalculateVelocityU7C7_0(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void Projectile_U3CLunchProjectileU3Eg__CalculateVelocityU7C7_0_mCD308882F5AEA12F3826B22D1C55B743B353607D (void);
// 0x0000002B System.Void Script.ForGame3.ScoreArea::Start()
extern void ScoreArea_Start_m680013A5AE9A24A06A629AF2212D64E8013A3267 (void);
// 0x0000002C System.Void Script.ForGame3.ScoreArea::OnTriggerEnter(UnityEngine.Collider)
extern void ScoreArea_OnTriggerEnter_m0DAC750FFC75E405F52AB7FE2C6790301A08812C (void);
// 0x0000002D System.Void Script.ForGame3.ScoreArea::scoreCount()
extern void ScoreArea_scoreCount_m0993C027BFF324A49A4FF2BF5723C924B25FC76B (void);
// 0x0000002E System.Void Script.ForGame3.ScoreArea::.ctor()
extern void ScoreArea__ctor_m0EC54E2CBC0A7B3479576D9F6887571F632AADFE (void);
// 0x0000002F System.Void Script.ForGame2.Block::FixedUpdate()
extern void Block_FixedUpdate_m960FCFD853B543FAC37E6ADBE22AFB3AF68B0553 (void);
// 0x00000030 System.Void Script.ForGame2.Block::Spawn()
extern void Block_Spawn_mD02312F186F6C3BDD96C611B50944635B11BA7F6 (void);
// 0x00000031 System.Void Script.ForGame2.Block::Cooldowns()
extern void Block_Cooldowns_m52594E06852228938F1F89B8F33064BCD96285A3 (void);
// 0x00000032 System.Void Script.ForGame2.Block::.ctor()
extern void Block__ctor_mE18E7D7C6C87B01B422A40FDD689B963D192853A (void);
// 0x00000033 System.Void Script.ForGame2.CheckHit::OnTriggerEnter(UnityEngine.Collider)
extern void CheckHit_OnTriggerEnter_m19AE429B406A8C783A4BFA06F9CC97381CF40CC1 (void);
// 0x00000034 System.Void Script.ForGame2.CheckHit::.ctor()
extern void CheckHit__ctor_m2B1B228902E56624E548E963FCEB5DAF0046D458 (void);
// 0x00000035 System.Void Script.ForGame2.FootballControl::Start()
extern void FootballControl_Start_mCA766F1E56B4742C4D2D51FAA9F2B25549A12510 (void);
// 0x00000036 System.Void Script.ForGame2.FootballControl::FixedUpdate()
extern void FootballControl_FixedUpdate_mF15E496BA5BDC3FB680944D8B2AB67FF09881545 (void);
// 0x00000037 System.Void Script.ForGame2.FootballControl::SaveData()
extern void FootballControl_SaveData_mA781186587287D920B890422FD3306A9D3742BBE (void);
// 0x00000038 System.Void Script.ForGame2.FootballControl::Movement()
extern void FootballControl_Movement_m8C44294598B9263B5D9B5A7E9A9A1F367763333B (void);
// 0x00000039 System.Void Script.ForGame2.FootballControl::Shoot()
extern void FootballControl_Shoot_mC89AA6770346F55A813E2A217575639A9F2DF4E4 (void);
// 0x0000003A System.Void Script.ForGame2.FootballControl::DeleteBlock()
extern void FootballControl_DeleteBlock_mEDDE84190BA1FC4A73AD834771A08F30CDFBB55D (void);
// 0x0000003B System.Void Script.ForGame2.FootballControl::OnTriggerEnter(UnityEngine.Collider)
extern void FootballControl_OnTriggerEnter_m32E61471099EC1F7C1A0571B4D99FF0864E24379 (void);
// 0x0000003C System.Void Script.ForGame2.FootballControl::.ctor()
extern void FootballControl__ctor_m07D1140C60C9E381973AC2C2B9D4C2381C29D9CC (void);
// 0x0000003D System.Void Script.ForGame2.GameManagerForPhysicsGameTwo::Start()
extern void GameManagerForPhysicsGameTwo_Start_mEA1B507200C2EE3ED5FF2D62119AC47C8C9C52F5 (void);
// 0x0000003E System.Void Script.ForGame2.GameManagerForPhysicsGameTwo::ChangeMenu(Script.ForGame2.GameManagerForPhysicsGameTwo/MenuType)
extern void GameManagerForPhysicsGameTwo_ChangeMenu_m52658DCEF109594258686889FEB0F34E115CE464 (void);
// 0x0000003F System.Collections.IEnumerator Script.ForGame2.GameManagerForPhysicsGameTwo::ChageMenuAnimation(UnityEngine.Vector3)
extern void GameManagerForPhysicsGameTwo_ChageMenuAnimation_m911048778188FA6B196E554CA744FE0AACD5804D (void);
// 0x00000040 System.Void Script.ForGame2.GameManagerForPhysicsGameTwo::OnSettingPowerButtonClicked()
extern void GameManagerForPhysicsGameTwo_OnSettingPowerButtonClicked_mF295454BA0C73F83C5E38E56A7F1D6957106B38A (void);
// 0x00000041 System.Void Script.ForGame2.GameManagerForPhysicsGameTwo::OnMainMenuButtonClicked()
extern void GameManagerForPhysicsGameTwo_OnMainMenuButtonClicked_m615232E4C39DDADDEA794494BBD8614F2EAA5D16 (void);
// 0x00000042 System.Void Script.ForGame2.GameManagerForPhysicsGameTwo::OnExitButtonClicked()
extern void GameManagerForPhysicsGameTwo_OnExitButtonClicked_m66BB80B866C47FA516EDB693191090632638788A (void);
// 0x00000043 System.Void Script.ForGame2.GameManagerForPhysicsGameTwo::OnRestartClicked()
extern void GameManagerForPhysicsGameTwo_OnRestartClicked_mD05B3ABF1C1BA0AED8A0BB28FC4C936DA2942D7C (void);
// 0x00000044 System.Void Script.ForGame2.GameManagerForPhysicsGameTwo::.ctor()
extern void GameManagerForPhysicsGameTwo__ctor_m718ADC7536164172658EC92AA140370E2B1DD62B (void);
// 0x00000045 System.Void Script.ForGame2.GameManagerForPhysicsGameTwo/<ChageMenuAnimation>d__5::.ctor(System.Int32)
extern void U3CChageMenuAnimationU3Ed__5__ctor_mB1042FE7AD81E3393D62C6F61F492A6FD7E5228F (void);
// 0x00000046 System.Void Script.ForGame2.GameManagerForPhysicsGameTwo/<ChageMenuAnimation>d__5::System.IDisposable.Dispose()
extern void U3CChageMenuAnimationU3Ed__5_System_IDisposable_Dispose_m802513110A080CD2BBAB5BBFFDCB56B0F1A6CAB4 (void);
// 0x00000047 System.Boolean Script.ForGame2.GameManagerForPhysicsGameTwo/<ChageMenuAnimation>d__5::MoveNext()
extern void U3CChageMenuAnimationU3Ed__5_MoveNext_m8CA47493D4B8431C9C2F601B1B65FBEE94258211 (void);
// 0x00000048 System.Object Script.ForGame2.GameManagerForPhysicsGameTwo/<ChageMenuAnimation>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CChageMenuAnimationU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mABEB0BB35A2FBB04E1171DC13E098090FDB9F554 (void);
// 0x00000049 System.Void Script.ForGame2.GameManagerForPhysicsGameTwo/<ChageMenuAnimation>d__5::System.Collections.IEnumerator.Reset()
extern void U3CChageMenuAnimationU3Ed__5_System_Collections_IEnumerator_Reset_m0B57C664864E6AF26A8B07246313C36721278AF2 (void);
// 0x0000004A System.Object Script.ForGame2.GameManagerForPhysicsGameTwo/<ChageMenuAnimation>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CChageMenuAnimationU3Ed__5_System_Collections_IEnumerator_get_Current_m883A822038A6A782011DC946D22B809193F75E01 (void);
// 0x0000004B System.Void Script.ForGame1.BulletControl::FixedUpdate()
extern void BulletControl_FixedUpdate_mC3EC51DCE8A6AE9E775419FD81820054F88F5CF4 (void);
// 0x0000004C System.Void Script.ForGame1.BulletControl::Fire()
extern void BulletControl_Fire_m57D372C37B785301B8E8D970ADCEB302EEC7C315 (void);
// 0x0000004D System.Void Script.ForGame1.BulletControl::.ctor()
extern void BulletControl__ctor_mB343B5E49D56DE6067185C1DEB11DFD0FB2F6D20 (void);
// 0x0000004E System.Void Script.ForGame1.GameManagerForPhysicsGameOne::OnExitButtonClicked()
extern void GameManagerForPhysicsGameOne_OnExitButtonClicked_mF53346443FDE4742D75FDF09D955D450990608E3 (void);
// 0x0000004F System.Void Script.ForGame1.GameManagerForPhysicsGameOne::OnRestartClicked()
extern void GameManagerForPhysicsGameOne_OnRestartClicked_mEB3E7AB5E8BC72F957C2DE86C1B60618D0C90C51 (void);
// 0x00000050 System.Void Script.ForGame1.GameManagerForPhysicsGameOne::.ctor()
extern void GameManagerForPhysicsGameOne__ctor_mFDAE70D6D2D5549C5362663C68E6500A3A5EE7D2 (void);
// 0x00000051 System.Void Script.ForGame1.PlayerController::Start()
extern void PlayerController_Start_m907F2717481953FAF924D03BBEFBE4A644721F3B (void);
// 0x00000052 System.Void Script.ForGame1.PlayerController::FixedUpdate()
extern void PlayerController_FixedUpdate_mC837DDD2B35E9D6FB6AEAA94FD0C1F74A910B456 (void);
// 0x00000053 System.Void Script.ForGame1.PlayerController::Move()
extern void PlayerController_Move_m2BF49FA37166D499F44FE73229626BB9E77F3E5F (void);
// 0x00000054 System.Void Script.ForGame1.PlayerController::.ctor()
extern void PlayerController__ctor_m904C957AFD915E73C24C4E9667364742B0A1B23A (void);
// 0x00000055 System.Void Script.ForGame1.RedCube::OnTriggerEnter(UnityEngine.Collider)
extern void RedCube_OnTriggerEnter_m62D87035896CAA77CBDDD236233A70CE5C72085B (void);
// 0x00000056 System.Void Script.ForGame1.RedCube::.ctor()
extern void RedCube__ctor_mFFC2B498956B24AB56A0A1D9CA5D845126DCF9F6 (void);
// 0x00000057 System.Void Script.ForGame1.YellowCube::OnTriggerEnter(UnityEngine.Collider)
extern void YellowCube_OnTriggerEnter_m1816EF6CF226503F18AD8FB967FD0B4C67F07CEA (void);
// 0x00000058 System.Void Script.ForGame1.YellowCube::.ctor()
extern void YellowCube__ctor_mE0F783B926590E2930DFA64400F962514C335A4F (void);
static Il2CppMethodPointer s_methodPointers[88] = 
{
	MenuControl_Start_mD21964394815531B6A097339BA0A03ABF4809F30,
	MenuControl_InitLevelButton_mE719A48CEF2CB7E67D9625583807D00D1C3C7DB0,
	MenuControl_ChangeMenu_m20609C8E4955A57406AE7CDF80B8A207DBC0D4E6,
	MenuControl_ChageMenuAnimation_mAF201D60BB6A22E57A3698BB07E80927111B8FF5,
	MenuControl_OnNumberSelect_mD33B819701C033AFE67F26590D985438BC2276BD,
	MenuControl_OnPlayButtonClicked_m3089A9C0309C357380BF7CE845CED7B47054989F,
	MenuControl_OnMainMenuButtonClicked_m748D718CC10175DE8AF551AB1AA50B75FBFA1BE2,
	MenuControl_OnCreditsButtonClicked_m213C6097EAE48714AD2AEA8CC12383EFFF95AAAE,
	MenuControl_Quit_m9400004B264D91198F1C6AFE6BEFED420D6F01FD,
	MenuControl__ctor_mA26ECD3901C87B4AB5E9791AFB59D40FA3079166,
	U3CU3Ec__DisplayClass6_0__ctor_m414E3E3630FCEBF3107F5A302085F366EB0D1958,
	U3CU3Ec__DisplayClass6_0_U3CInitLevelButtonU3Eb__0_mC63D3EEAFEB20BD38FAE870471FDBE040035C7C7,
	U3CChageMenuAnimationU3Ed__8__ctor_mBFB3BD211076C6EE28A15B82D5F83A883B483B50,
	U3CChageMenuAnimationU3Ed__8_System_IDisposable_Dispose_mB512C8B02E3C3B5AEC0467D9A3393175586CAD45,
	U3CChageMenuAnimationU3Ed__8_MoveNext_mCB4C2F21463D83F21A260163BEA58FAA8E9C346F,
	U3CChageMenuAnimationU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC5F748F0211E70047D28296E465BFAA02294C011,
	U3CChageMenuAnimationU3Ed__8_System_Collections_IEnumerator_Reset_m9F62FFDE0B9D9FC68CD7D709F4F5E650EC671D81,
	U3CChageMenuAnimationU3Ed__8_System_Collections_IEnumerator_get_Current_m3B641347E2DD11B0A2999A902D1A5F2FF81C8AD9,
	MenuSound_Awake_mAB024C294FD0DEACA600BF2E46E6B189A37D843B,
	MenuSound__ctor_m04E1F731E6AB3FB0B9D46FEBF54DDEF376ADD8B8,
	BulletScript_Start_mA34DCEA3EEF4B4C59C7894A2BF88A2CBB3E41511,
	BulletScript_Update_m496194453396775E3E37212315732887208C1944,
	BulletScript_LunchProjectile_m9EF6D6E2D58252EA1A988DC6C2E6968498AEFD00,
	BulletScript__ctor_m20F8377E387D8E55B9CE61D63A6F120491921887,
	BulletScript_U3CLunchProjectileU3Eg__CalculateVelocityU7C9_0_m2B6CBE5A42025E49EE25EBAA29D6E5A8FE8382A9,
	DeleteBall_OnTriggerEnter_m6DC52D6F2528718C4A3819C244E6E069EF45B815,
	DeleteBall__ctor_mBAA6EB20A436C2E5048D523B0EC13C4060F3CDA3,
	GameManagerForPhysicsGameThree_FixedUpdate_m84A94F5FB54763D12AF7F336BCC423EE3B272F0C,
	GameManagerForPhysicsGameThree_OnExitButtonClicked_m8CA1ED1E3FB7844ECE033307CA6852C9688C9123,
	GameManagerForPhysicsGameThree_OnRestartClicked_m8D4AF0AFBD7F252935196699644154C05C5276EC,
	GameManagerForPhysicsGameThree__ctor_m58FD7CC710E8E77CDC40D3914C0E7BA3D5B07DEB,
	MouseLook_Start_m514114534D96AC00B531892234D557EA58F3477E,
	MouseLook_Update_mFFAFF4FD2868081BE4503937F387FF7AB5FCCC5A,
	MouseLook__ctor_m9396072602E811AB09C7B5C03D19E53A77C00BB5,
	PlayerMovement_Update_m241BC416BFDC91F1C8AF01230836C4C37B21082B,
	PlayerMovement__ctor_m0A7C744B933C4B2C145863109D88BFD6A4375453,
	Projectile_Start_mE3048ED065CD6782748B980B2C56B323E5030774,
	Projectile_Update_m49DC62A3C68EB106AD225059EC2A8FC194077ED7,
	Projectile_LunchProjectile_m3E40E37398D764B65591C76B4B74E2E050587764,
	Projectile_OnTriggerEnter_m522E8CDC74F5C291240BB919C16923E2851BB3F9,
	Projectile__ctor_m593C60F79D70FC56CBA9374281744FA43017EF48,
	Projectile_U3CLunchProjectileU3Eg__CalculateVelocityU7C7_0_mCD308882F5AEA12F3826B22D1C55B743B353607D,
	ScoreArea_Start_m680013A5AE9A24A06A629AF2212D64E8013A3267,
	ScoreArea_OnTriggerEnter_m0DAC750FFC75E405F52AB7FE2C6790301A08812C,
	ScoreArea_scoreCount_m0993C027BFF324A49A4FF2BF5723C924B25FC76B,
	ScoreArea__ctor_m0EC54E2CBC0A7B3479576D9F6887571F632AADFE,
	Block_FixedUpdate_m960FCFD853B543FAC37E6ADBE22AFB3AF68B0553,
	Block_Spawn_mD02312F186F6C3BDD96C611B50944635B11BA7F6,
	Block_Cooldowns_m52594E06852228938F1F89B8F33064BCD96285A3,
	Block__ctor_mE18E7D7C6C87B01B422A40FDD689B963D192853A,
	CheckHit_OnTriggerEnter_m19AE429B406A8C783A4BFA06F9CC97381CF40CC1,
	CheckHit__ctor_m2B1B228902E56624E548E963FCEB5DAF0046D458,
	FootballControl_Start_mCA766F1E56B4742C4D2D51FAA9F2B25549A12510,
	FootballControl_FixedUpdate_mF15E496BA5BDC3FB680944D8B2AB67FF09881545,
	FootballControl_SaveData_mA781186587287D920B890422FD3306A9D3742BBE,
	FootballControl_Movement_m8C44294598B9263B5D9B5A7E9A9A1F367763333B,
	FootballControl_Shoot_mC89AA6770346F55A813E2A217575639A9F2DF4E4,
	FootballControl_DeleteBlock_mEDDE84190BA1FC4A73AD834771A08F30CDFBB55D,
	FootballControl_OnTriggerEnter_m32E61471099EC1F7C1A0571B4D99FF0864E24379,
	FootballControl__ctor_m07D1140C60C9E381973AC2C2B9D4C2381C29D9CC,
	GameManagerForPhysicsGameTwo_Start_mEA1B507200C2EE3ED5FF2D62119AC47C8C9C52F5,
	GameManagerForPhysicsGameTwo_ChangeMenu_m52658DCEF109594258686889FEB0F34E115CE464,
	GameManagerForPhysicsGameTwo_ChageMenuAnimation_m911048778188FA6B196E554CA744FE0AACD5804D,
	GameManagerForPhysicsGameTwo_OnSettingPowerButtonClicked_mF295454BA0C73F83C5E38E56A7F1D6957106B38A,
	GameManagerForPhysicsGameTwo_OnMainMenuButtonClicked_m615232E4C39DDADDEA794494BBD8614F2EAA5D16,
	GameManagerForPhysicsGameTwo_OnExitButtonClicked_m66BB80B866C47FA516EDB693191090632638788A,
	GameManagerForPhysicsGameTwo_OnRestartClicked_mD05B3ABF1C1BA0AED8A0BB28FC4C936DA2942D7C,
	GameManagerForPhysicsGameTwo__ctor_m718ADC7536164172658EC92AA140370E2B1DD62B,
	U3CChageMenuAnimationU3Ed__5__ctor_mB1042FE7AD81E3393D62C6F61F492A6FD7E5228F,
	U3CChageMenuAnimationU3Ed__5_System_IDisposable_Dispose_m802513110A080CD2BBAB5BBFFDCB56B0F1A6CAB4,
	U3CChageMenuAnimationU3Ed__5_MoveNext_m8CA47493D4B8431C9C2F601B1B65FBEE94258211,
	U3CChageMenuAnimationU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mABEB0BB35A2FBB04E1171DC13E098090FDB9F554,
	U3CChageMenuAnimationU3Ed__5_System_Collections_IEnumerator_Reset_m0B57C664864E6AF26A8B07246313C36721278AF2,
	U3CChageMenuAnimationU3Ed__5_System_Collections_IEnumerator_get_Current_m883A822038A6A782011DC946D22B809193F75E01,
	BulletControl_FixedUpdate_mC3EC51DCE8A6AE9E775419FD81820054F88F5CF4,
	BulletControl_Fire_m57D372C37B785301B8E8D970ADCEB302EEC7C315,
	BulletControl__ctor_mB343B5E49D56DE6067185C1DEB11DFD0FB2F6D20,
	GameManagerForPhysicsGameOne_OnExitButtonClicked_mF53346443FDE4742D75FDF09D955D450990608E3,
	GameManagerForPhysicsGameOne_OnRestartClicked_mEB3E7AB5E8BC72F957C2DE86C1B60618D0C90C51,
	GameManagerForPhysicsGameOne__ctor_mFDAE70D6D2D5549C5362663C68E6500A3A5EE7D2,
	PlayerController_Start_m907F2717481953FAF924D03BBEFBE4A644721F3B,
	PlayerController_FixedUpdate_mC837DDD2B35E9D6FB6AEAA94FD0C1F74A910B456,
	PlayerController_Move_m2BF49FA37166D499F44FE73229626BB9E77F3E5F,
	PlayerController__ctor_m904C957AFD915E73C24C4E9667364742B0A1B23A,
	RedCube_OnTriggerEnter_m62D87035896CAA77CBDDD236233A70CE5C72085B,
	RedCube__ctor_mFFC2B498956B24AB56A0A1D9CA5D845126DCF9F6,
	YellowCube_OnTriggerEnter_m1816EF6CF226503F18AD8FB967FD0B4C67F07CEA,
	YellowCube__ctor_mE0F783B926590E2930DFA64400F962514C335A4F,
};
static const int32_t s_InvokerIndices[88] = 
{
	1452,
	1452,
	1205,
	954,
	1205,
	1452,
	1452,
	1452,
	1452,
	1452,
	1452,
	1452,
	1205,
	1452,
	1430,
	1410,
	1452,
	1410,
	1452,
	1452,
	1452,
	1452,
	1452,
	1452,
	1880,
	1215,
	1452,
	1452,
	1452,
	1452,
	1452,
	1452,
	1452,
	1452,
	1452,
	1452,
	1452,
	1452,
	1452,
	1215,
	1452,
	1880,
	1452,
	1215,
	1452,
	1452,
	1452,
	1452,
	1452,
	1452,
	1215,
	1452,
	1452,
	1452,
	1452,
	1452,
	1452,
	1452,
	1215,
	1452,
	1452,
	1205,
	954,
	1452,
	1452,
	1452,
	1452,
	1452,
	1205,
	1452,
	1430,
	1410,
	1452,
	1410,
	1452,
	1452,
	1452,
	1452,
	1452,
	1452,
	1452,
	1452,
	1452,
	1452,
	1215,
	1452,
	1215,
	1452,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	88,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
