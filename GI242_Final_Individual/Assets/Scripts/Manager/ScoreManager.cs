﻿    using TMPro;
using UnityEngine;

namespace Manager
{
    public class ScoreManager : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private TextMeshProUGUI finalScoreText;

        private GameManager gameManager;
        private int playerScore;

        public void Init(GameManager gameManager)
        {
            this.gameManager = gameManager;
            this.gameManager.OnRestarted += OnRestarted;
            HideScore(false);
            SetScore(0);
            finalScoreText.text = "";
        }
        public void SetScore(int score)
        {
            scoreText.text = $"Score : {score}";
            playerScore = score;
        }

        private void Awake()
        {
            Debug.Assert(scoreText != null, "scoreText cannot be null");
            //Debug.Assert(finalScoreText != null, "finalScoreText cannot be null");
        }

        private void OnRestarted()
        {
            finalScoreText.text = $"Player Score : {playerScore}";
            gameManager.OnRestarted -= OnRestarted;
            HideScore(true);
            SetScore(0);
        }

        private void HideScore(bool hide)
        {
            scoreText.gameObject.SetActive(!hide);
        }
    }
}