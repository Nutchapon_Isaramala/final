﻿using System;
using Enemy;
using Spaceship;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Manager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private Button startButton;
        [SerializeField] private RectTransform dialog;
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private BossSpaceShip bossSpaceship;
        [SerializeField] private ScoreManager scoreManager;
        public event Action OnRestarted;
        [SerializeField] private int playerSpaceshipHp;
        [SerializeField] private int playerSpaceshipMoveSpeed;
        [SerializeField] private int enemySpaceshipHp;
        [SerializeField] private int enemySpaceshipMoveSpeed;
        [SerializeField] private int bossSpaceshipHp;
        [SerializeField] private int bossSpaceshipMoveSpeed;

        [SerializeField] private TextMeshProUGUI buttonText;
        public PlayerSpaceship spawnedPlayerShip;
        
        private int score = 0;
        
        public static GameManager Instance { get; private set;}
        private void Awake()
        {
            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(dialog != null, "dialog cannot be null");
            Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
            Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
            Debug.Assert(scoreManager != null, "scoreManager cannot be null");
            Debug.Assert(playerSpaceshipHp > 0, "playerSpaceship hp has to be more than zero ");
            Debug.Assert(playerSpaceshipMoveSpeed > 0, "playerSpaceshipMoveSpeed has to be more than zero ");
            Debug.Assert(enemySpaceshipHp > 0, "enemySpaceshipHp hp has to be more than zero ");
            Debug.Assert(enemySpaceshipMoveSpeed > 0, "enemySpaceshipMoveSpeed  has to be more than zero ");
            
            buttonText.text = "Start";
            startButton.onClick.AddListener(OnStartButtonClicked);
            SoundManager.Instance.PlayBGM();
            if (Instance == null)
            {
                Instance = this;
            }
            DontDestroyOnLoad(this);
        }
        private void OnStartButtonClicked()    
        {
            dialog.gameObject.SetActive(false);
            StartGame();
        }
        private void StartGame()
        {
            scoreManager.Init(this);
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();
            SoundManager.Instance.PlayBGM();
        }
        private void SpawnPlayerSpaceship()
        {
            spawnedPlayerShip = Instantiate(playerSpaceship);
            spawnedPlayerShip.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            spawnedPlayerShip.OnExploded += OnPlayerSpaceshipExploded;
        }
        private void OnPlayerSpaceshipExploded()
        {
            score = 0;
            Restart();
            buttonText.text = "Restart";
        }

        private void SpawnEnemySpaceship()
        {
            var spawnedEnemyShip = Instantiate(enemySpaceship);
            spawnedEnemyShip.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed);
            spawnedEnemyShip.OnExploded += OnEnemySpaceshipExploded;
            
        }
        private void SpawnBossSpaceship()
        {
            var spawnedBossShip = Instantiate(bossSpaceship);
            spawnedBossShip.Init(bossSpaceshipHp, bossSpaceshipMoveSpeed);
            spawnedBossShip.OnExploded += OnBossSpaceshipExploded;
        }
        
        private void OnEnemySpaceshipExploded()
        {
            var random = Random.Range(1,10);
            score++;
            scoreManager.SetScore(score);
            
            if (random == 1)
            {
                SpawnBossSpaceship();
            }
            else
            {
                SpawnEnemySpaceship();
            }
            
        }
        private void OnBossSpaceshipExploded()
        {
            score++;
            scoreManager.SetScore(score);
            OnEnemySpaceshipExploded();
        }


        private void Restart()
        {
            DestroyRemainingShips();
            dialog.gameObject.SetActive(true);
            OnRestarted.Invoke();
        }

        private void DestroyRemainingShips()
        {
            var remainingEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in remainingEnemies)
            {
                Destroy(enemy);
            }
            var remainingBoss = GameObject.FindGameObjectsWithTag("Boss");
            foreach (var boss in remainingBoss)
            {
                Destroy(boss);
            }

            var remainingPlayers = GameObject.FindGameObjectsWithTag("Player");
            foreach (var player in remainingPlayers)
            {
                Destroy(player);
            }
        }
    }
}
