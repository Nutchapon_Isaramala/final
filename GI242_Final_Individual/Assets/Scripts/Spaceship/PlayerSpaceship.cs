﻿using System;
using Manager;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Spaceship
{

    public class PlayerSpaceship : Basespaceship, IDamagable
    {
        [SerializeField] private AudioClip playerExplodedSound;
        [SerializeField] private float playerExplodedSoundVolume = 0.1f;
        [SerializeField] private TextMeshProUGUI text;
        public event Action OnExploded;
        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");

            audioSource = GetComponent<AudioSource>();
            
        }
        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public override void Fire()
        {
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            bullet.Init(Vector2.up);
            SoundManager.Instance.Play(audioSource,SoundManager.Sound.PlayerFire);

        }
        public void TakeHit(int damage)
        {

            Hp -= damage;
            if (Hp > 0)
            {
                return;
            }
            Explode();
        }
        public void Explode()
        {
            AudioSource.PlayClipAtPoint(playerExplodedSound, Camera.main.transform.position, playerExplodedSoundVolume);
            Debug.Assert(Hp <= 0, "Hp is more than zero");
            Destroy(gameObject);
            OnExploded?.Invoke();
        }
        
    }
}