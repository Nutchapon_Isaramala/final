using System;
using Manager;
using Spaceship;
using UnityEngine;

namespace Enemy
{
    public class BossController : MonoBehaviour
    {
        [SerializeField] private BossSpaceShip bossSpaceship;
        [SerializeField] private float chasingThresholdDistance;
        
        private PlayerSpaceship spawnedPlayerShip;
        private void Awake()
        {
            spawnedPlayerShip = GameManager.Instance.spawnedPlayerShip;
        }

        private void Update()
        {
            MoveToPlayer();
            bossSpaceship.Fire();
        }


        private void MoveToPlayer()
        {
            var distanceToPlayer = Vector2.Distance(spawnedPlayerShip.transform.position, transform.position);
            if (distanceToPlayer < chasingThresholdDistance)
            {
                var direction = (Vector2)(spawnedPlayerShip.transform.position - transform.position);
                direction.Normalize();
                var distance = direction * bossSpaceship.Speed * Time.deltaTime / 2;
                gameObject.transform.Translate(distance);
            }
        }
    }    
}
